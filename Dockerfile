##
## BUILD image
##
## FIXME: build-stage muss auf "golang:1.13" laufen - einem debian oder ubuntu
## die binary sollte dann allerdings ebenso auf alpine funktionieren
## => den build-image schritt umbauen

ARG ARCH=amd64
FROM $ARCH/golang:1.13 AS build-image

RUN apk --no-cache --no-progress add libmagic-dev libpng-dev libjpeg-dev libtiff-dev

RUN mkdir -p /go/src/gitlab.com/mbalser1 \
  && cd /go/src/gitlab.com/mbalser1 \
  && git clone https://gitlab.com/mbalser1/findimagedupes.git \
  && GOPATH=/go go mod init \
  && cd findimagedupes \
  && GOPATH=/go go get -v ./...

RUN ls -al /go/bin/

##
## RUNTIME image
##
ARG ARCH=amd64
FROM $ARCH/alpine:3 AS runtime-image

MAINTAINER Michael Balser <michael@balser.cc>

# args
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Michael Balser <michael@balser.cc>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="michaelbalser/avif" \
  org.label-schema.description="avif docker image" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/repository/docker/michaelbalser/avif" \
  org.label-schema.vcs-url="https://gitlab.com/mbalser1/avif" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install packages
RUN apk --no-cache --no-progress add tini

COPY --from=build-image /go/bin/findimagedupes /findimagedupes

ENTRYPOINT ["/sbin/tini", "--", "/findimagedupes"]
